package servlet;

import commons.dico.ArrayListEmptyWords;
import commons.dico.EmptyWords;
import commons.math.FloatVector;
import commons.math.VectorFloatVector;
import commons.pagewordrelation.ArrayListPageWordsRelation;
import commons.pagewordrelation.IndexToPage;
import commons.pagewordrelation.PageWordsRelation;
import commons.pagewordrelation.SimpleIndexToPage;

public class DataPagerank {
	/*
	 * Singleton part
	 */
	private static DataPagerank instance;
	public static DataPagerank getInstance() {
		if (instance == null)
			instance = new DataPagerank();
		return instance;
	}
	private DataPagerank() {
		indexToPage = new SimpleIndexToPage();
		indexToPage.read(pathfileIndexToPage);
		pageWordsRelation = new ArrayListPageWordsRelation();
		pageWordsRelation.read(pathfilePageWordsRelation);
		pageRankVector = new VectorFloatVector();
		pageRankVector.read(pathfilePageRankVector);
		emptyWords = new ArrayListEmptyWords();
		emptyWords.read(pathfileEmptyWords);
	}
	
	/*
	 * Instance part
	 */
	private String pathfileIndexToPage = "";
	private IndexToPage indexToPage;
	private String pathfilePageWordsRelation = "";
	private PageWordsRelation pageWordsRelation;
	private String pathfilePageRankVector = "";
	private FloatVector pageRankVector;
	private String pathfileEmptyWords = "";
	private EmptyWords emptyWords;
	
	public IndexToPage getIndexToPage() {
		return indexToPage;
	}
	public PageWordsRelation getPageWordsRelation() {
		return pageWordsRelation;
	}
	public FloatVector getPageRankVector() {
		return pageRankVector;
	}
	public EmptyWords getEmptyWords() {
		return emptyWords;
	}
}
