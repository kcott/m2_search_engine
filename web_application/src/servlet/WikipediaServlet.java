package servlet;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import search.Request;


/**
 * Servlet implementation class WikipediaServlet
 */
public class WikipediaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * Default constructor. 
     */
    public WikipediaServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		writeHead(response);
		writeResearchForm(response);
		writeEnd(response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// Get the research
		String research = replaceAccents(request.getParameter("search"));
		ArrayList<String> listWords = new ArrayList<String>();
		for(String word:research.split(" ")) {
			listWords.add(word);
		}
		
		// Clean the user research on well-formed research
		for(int index=listWords.size(); 0<=index; index--) {
			if(DataPagerank.getInstance().getEmptyWords().contains(listWords.get(index))) {
				listWords.remove(index);
			}
		}
		String[] words = new String[listWords.size()];
		for(int index=0; index<listWords.size(); index++) {
			words[index] = listWords.get(index);
		}
		
		// Get the result of the research
		Request req = new Request();
		List<String> pages = req.request(
				DataPagerank.getInstance().getIndexToPage(),
				DataPagerank.getInstance().getPageWordsRelation(),
				DataPagerank.getInstance().getPageRankVector(),
				words);
		writeHead(response);
		writeResearchForm(response);
		writeResearchPages(response, pages);
		writeEnd(response);
	}
	
	protected void writeHead (HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().print("<!DOCTYPE html><html><body>");
	}
	
	protected void writeEnd (HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().print("</body></html>");
	}
	
	protected void writeResearchForm (HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().print(
				"<form id=\"researchForm\" action=\"\" method=\"post\" >"
					+ "<input id=\"search\" name=\"search\"/>"
					+ "<button type=\"submin\">"
						+ "search"
					+ "</button>"
				+ "</form>"
		);
	}

	protected void writeResearchPages (HttpServletResponse response, List<String> pages) throws ServletException, IOException {
		for (String page:pages) {
			response.getWriter().print(""
					+ "<a href=\""+page+"\">");
		}
	}
	
	private String replaceAccents(String string) {
	    String nfdNormalizedString = Normalizer.normalize(string, Normalizer.Form.NFD); 
	    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
	    return pattern.matcher(nfdNormalizedString).replaceAll("");
	}
}
