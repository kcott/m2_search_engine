package search;

import java.util.ArrayList;
import java.util.List;

import commons.math.FloatVector;
import commons.pagewordrelation.IndexToPage;
import commons.pagewordrelation.PageWordsRelation;

public class Request {
	/**
	 * Return a list of pages to the word in request 
	 * @param indexToPage IndexToPage
	 * @param pageWordsRelation PageWordsRelation
	 * @param pageRankVector FloatVector
	 * @param words String[]
	 * @return List<String>
	 */
	public List<String> request(
			IndexToPage indexToPage,
			PageWordsRelation pageWordsRelation,
			FloatVector pageRankVector,
			String[] words) {
		List<String> pages = new ArrayList<String>();
		int[] indexForWords = new int[words.length];
		
		for (int i=0; i<indexForWords.length; i++) {
			indexForWords[i] = 0;
		}
		
		// Sorts table words by crescent size of page number
		String tmpWord = "";
		for (int word1=1; word1<words.length; word1++) {
			for (int word2=word1-1; 0<=word2; word2--) {
				if (pageRankVector.get(word1) < pageRankVector.get(word2)) {
					tmpWord = words[word1];
					words[word1] = words[word2];
					words[word2] = tmpWord;
				} else {
					break;
				}
			}
		}
		
		int word = 0; // to word[1] at word[n]
		boolean addPage = false;
		
		// Gets page identifiers
		algorithm:
		while(indexForWords[0] < pageWordsRelation.getPageNumberForWord(words[0])) {
			addPage = true;
			loop_on_words:
			for (word = 1; word < words.length; word++) {
				while (indexForWords[word] < pageWordsRelation.getPageNumberForWord(words[word])) {
					if (pageWordsRelation.getPage(words[0], indexForWords[0])
							== pageWordsRelation.getPage(words[word], indexForWords[word])) {
						continue loop_on_words;
					} else if (pageRankVector.get(pageWordsRelation.getPage(words[word], indexForWords[word]))
							< pageRankVector.get(pageWordsRelation.getPage(words[0], indexForWords[0]))) {
						addPage = false;
						break loop_on_words;
					}
					indexForWords[word]++;
				}
				if (pageWordsRelation.getPageNumberForWord(words[word]) <= indexForWords[word]) {
					break algorithm;
				}
			}
			if (addPage == true) {
				pages.add(indexToPage.get(pageWordsRelation.getPage(words[0], indexForWords[0])));
			}
			indexForWords[0]++;
		}
		return pages;
	}
}
