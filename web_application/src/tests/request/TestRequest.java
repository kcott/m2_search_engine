package tests.request;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import search.Request;
import commons.math.FloatVector;
import commons.math.VectorFloatVector;
import commons.pagewordrelation.ArrayListPageWordsRelation;
import commons.pagewordrelation.IndexToPage;
import commons.pagewordrelation.PageWordsRelation;
import commons.pagewordrelation.SimpleIndexToPage;

public class TestRequest {
	private static IndexToPage indexToPage;
	private static PageWordsRelation pageWordsRelation;
	private static FloatVector pageRankVector;
	private static int NUMBER = 11;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		indexToPage = new SimpleIndexToPage();
		for (int i=0; i<NUMBER; i++) {
			indexToPage.set(i, "page"+i);
		}
		
		pageWordsRelation = new ArrayListPageWordsRelation();
		for (int i=0; i<NUMBER; i++) {
			pageWordsRelation.add("all", i);
		}
		for (int i=0; i<NUMBER/2; i++) {
			pageWordsRelation.add("even", i*2); // pair
		}
		for (int i=0; i<NUMBER/2; i++) {
			pageWordsRelation.add("odd", i*2+1); // impair
		}
		
		pageRankVector = new VectorFloatVector(NUMBER);
		for (int i=0; i<NUMBER; i++) {
			pageRankVector.add(NUMBER-i);
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRequestEven() {
		Request request = new Request();
		String[] words = {"all", "even"};
		List<String> pages = request.request(indexToPage, pageWordsRelation, pageRankVector, words);
		
		int n = NUMBER/2;
		assertEquals("testRequest even number pages", n, pages.size());
		for (int i=0; i<n; i++) {
			assertEquals("testRequest even page "+(i*2), "page"+(i*2), pages.get(i));
		}
	}

	@Test
	public void testRequestOdd() {
		Request request = new Request();
		String[] words = {"all", "odd"};
		List<String> pages = request.request(indexToPage, pageWordsRelation, pageRankVector, words);
		
		int n = NUMBER/2;
		assertEquals("testRequest odd number pages", n, pages.size());
		for (int i=0; i<n; i++) {
			assertEquals("testRequest odd page "+(i*2+1), "page"+(i*2+1), pages.get(i));
		}
	}
}
