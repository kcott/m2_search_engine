package tests.commons.matrix;

import commons.math.FloatVector;
import commons.math.VectorFloatVector;
import commons.math.matrix.ArrayCLI;
import commons.math.matrix.ArrayListCLI;
import commons.math.matrix.CLI;

public class PerformancesCLIArrayListVSArray {
	public static void main(String argv[]) {
		long start = 0;
		long stop = 0;
		
		int LINES = 2000000;
		int VALUES_BY_LINES = 25;
		
		/*
		 * Test time to create the array matrix
		 */
		start = System.currentTimeMillis();
		CLI matrix = new ArrayListCLI();
		for(int l=0; l<LINES; l++) { // line
			/*for(int c=0; c<VALUES_BY_LINES ; c++) { // column
				matrix.addContent(l, c, 0);
			}*/
			for(int c=VALUES_BY_LINES; 0<=c ; c--) { // column
				matrix.addContent(l, c, 0);
			}
		}
		stop = System.currentTimeMillis();

		System.out.println("time to fill "+LINES+" lines and "+VALUES_BY_LINES+" values by lines:"+(stop-start)+"ms");
		System.out.println("\t"+(stop-start)+"ms");
		System.out.println("\t"+(stop-start)/1000+"s");
		
		// Create a random vector to test the time of multiply functions 
		FloatVector vector = new VectorFloatVector(LINES);
		for(int l=0; l<LINES; l++) {
			vector.add((float) (Math.random()*10));
		}
		
		/*
		 * Test time to multiply a matrix and a vector with array list
		 */
		start = System.currentTimeMillis();
		matrix.multiply(vector);
		stop = System.currentTimeMillis();
		System.out.println("matrix * vector in "+(stop-start)+"ms");

		/*
		 * Test time to transform a vector with array list to vector with array 
		 */
		start = System.currentTimeMillis();
		CLI matrixWithArray = new ArrayCLI(
				((ArrayListCLI)matrix).getContents(),
				((ArrayListCLI)matrix).getNewLines(),
				((ArrayListCLI)matrix).getColumnIndexes());
		stop = System.currentTimeMillis();
		System.out.println("array list matrix -> array matrix in "+(stop-start));

		/*
		 * Test time to multiply a matrix and a vector with array
		 */
		start = System.currentTimeMillis();
		matrixWithArray.multiply(vector);
		stop = System.currentTimeMillis();
		System.out.println("matrix * vector in "+(stop-start)+"ms");
	}
}
