package tests.commons.matrix;

import commons.math.FloatVector;
import commons.math.VectorFloatVector;
import commons.math.matrix.ArrayCLI;
import commons.math.matrix.CLI;

public class PerformancesCLIArray {
	public static void main(String argv[]) {
		long start = 0;
		long stop = 0;
		
		int LINES = 2000000;
		int VALUES_BY_LINES = 25;
		
		/*
		 * Test time to create the matrix
		 */
		start = System.currentTimeMillis();
		CLI matrix = new ArrayCLI(LINES*VALUES_BY_LINES, LINES);
		for(int l=0; l<LINES; l++) { // line
			for(int c=0; c<VALUES_BY_LINES ; c++) { // column
				matrix.addContent(l, c, 0);
			}
			/*for(int c=VALUES_BY_LINES; 0<=c ; c--) { // column
				matrix.addContent(l, c, 0);
			}*/
		}
		stop = System.currentTimeMillis();

		System.out.println("time to fill "+LINES+" lines and "+VALUES_BY_LINES+" values by lines:"+(stop-start)+"ms");
		System.out.println("\t"+(stop-start)+"ms");
		System.out.println("\t"+(stop-start)/1000+"s");
		
		// Create a random vector to test the time of multiply functions 
		FloatVector vector = new VectorFloatVector(LINES);
		for(int l=0; l<LINES; l++) {
			vector.add((float) (Math.random()*10));
		}
		
		/*
		 * Test time to multiply a matrix and a vector
		 */
		start = System.currentTimeMillis();
		matrix.multiply(vector);
		stop = System.currentTimeMillis();
		System.out.println("matrix * vector in "+(stop-start)+"ms");
		
		/*
		 * Test time to multiply the transposed of a matrix and a vector
		 */
		start = System.currentTimeMillis();
		matrix.multiplyByTransposed(vector);
		stop = System.currentTimeMillis();
		System.out.println("transposed_matrix * vector in "+(stop-start)+"ms");
	}
}
