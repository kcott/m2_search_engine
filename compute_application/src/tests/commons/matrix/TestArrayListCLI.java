package tests.commons.matrix;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import commons.math.FloatVector;
import commons.math.VectorFloatVector;
import commons.math.matrix.ArrayListCLI;
import commons.math.matrix.SparseMatrix;

public class TestArrayListCLI {
	private ArrayListCLI matrix;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		matrix = new ArrayListCLI();
		matrix.addContent(3, 1, 3);
		matrix.addContent(1, 2, 2);
		matrix.addContent(0, 1, 3);
		matrix.addContent(0, 2, 5);
		matrix.addContent(1, 0, 1);
		matrix.addContent(0, 3, 8);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMatrixCreation() {
		float[] expectedContents = {(float) 3, (float) 5, (float) 8, (float) 1, (float) 2, (float) 3};
		float[] contents = matrix.getContents();
		
		int[] expectedLines = {0, 3, 5, 5, 6};
		int[] lines = matrix.getNewLines();
		
		int[] expectedColumnsIndexes = {1, 2, 3, 0, 2, 1};
		int[] columnIndexes = matrix.getColumnIndexes();
		
		assertArrayEquals("testMatrixCreation contents", expectedContents, contents, 0.0f);
		assertArrayEquals("testMatrixCreation lines", expectedLines, lines);
		assertArrayEquals("testMatrixCreation column indexes", expectedColumnsIndexes, columnIndexes);
	}
	
	@Test
	public void testMatrixMultiplyVector() {
		FloatVector vector = new VectorFloatVector();
		vector.add((float) 0);
		vector.add((float) 1);
		vector.add((float) 0);
		vector.add((float) 2);
		vector.add((float) 2);
		
		FloatVector result = new VectorFloatVector();
		result.add((float) 19);
		result.add((float) 0);
		result.add((float) 0);
		result.add((float) 3);
		result.add((float) 0);
		
		assertEquals("testMatrixMultiplyVector vector", result, matrix.multiply(vector));
	}
	
	@Test
	public void testMatrixMultiplyByInverseVector() {
		FloatVector vector = new VectorFloatVector();
		vector.add((float) 1);
		vector.add((float) 2);
		vector.add((float) 3);
		vector.add((float) 4);
		
		FloatVector result = new VectorFloatVector();
		result.add((float) 2);
		result.add((float) 15);
		result.add((float) 9);
		result.add((float) 8);
		
		assertEquals("testMatrixMultiplyByTransposedVector vector", result, matrix.multiplyByTransposed(vector));
	}
	
	@Test
	public void testGetCell() {
		assertEquals("testGetCell 3 1", 3, matrix.getCell(3, 1), 0.0f);
		assertEquals("testGetCell 1 2", 2, matrix.getCell(1, 2), 0.0f);
		assertEquals("testGetCell 0 1", 3, matrix.getCell(0, 1), 0.0f);
		assertEquals("testGetCell 0 2", 5, matrix.getCell(0, 2), 0.0f);
		assertEquals("testGetCell 1 0", 1, matrix.getCell(1, 0), 0.0f);
		assertEquals("testGetCell 0 3", 8, matrix.getCell(0, 3), 0.0f);

		assertEquals("testGetCell 0 0", 0, matrix.getCell(0, 0), 0.0f);
		assertEquals("testGetCell 10 10", 0, matrix.getCell(10, 10), 0.0f);
	}
	
	@Test
	public void testLineSize() {
		assertEquals("testLineSize getLineSize", 4, matrix.getLineSize());
		assertEquals("testLineSize getColumnSize", 4, matrix.getColumnSize());
		
		assertEquals("testLineSize getLineSize(int) 0", 3, matrix.getLineSize(0));
		assertEquals("testLineSize getLineSize(int) 1", 2, matrix.getLineSize(1));
		assertEquals("testLineSize getLineSize(int) 2", 0, matrix.getLineSize(2));
		assertEquals("testLineSize getLineSize(int) 3", 1, matrix.getLineSize(3));
	}
	
	@Test
	public void testGetColumnIndexesOfLine() {
		if (!(matrix instanceof SparseMatrix))
			throw new RuntimeException("testGetColumnIndexesOfLine matrix must be an instanceof SparseMatrix");
		
		int[] expectedNeighbours = null;
		
		expectedNeighbours = new int[3];
		expectedNeighbours[0] = 1;
		expectedNeighbours[1] = 2;
		expectedNeighbours[2] = 3;
		assertArrayEquals("testGetColumnIndexesOfLine 0", expectedNeighbours, ((SparseMatrix)matrix).getColumnIndexesOfLine(0));

		expectedNeighbours = new int[2];
		expectedNeighbours[0] = 0;
		expectedNeighbours[1] = 2;
		assertArrayEquals("testGetColumnIndexesOfLine 1", expectedNeighbours, ((SparseMatrix)matrix).getColumnIndexesOfLine(1));

		expectedNeighbours = new int[0];
		assertArrayEquals("testGetColumnIndexesOfLine 2", expectedNeighbours, ((SparseMatrix)matrix).getColumnIndexesOfLine(2));
		
		expectedNeighbours = new int[1];
		expectedNeighbours[0] = 1;
		assertArrayEquals("testGetColumnIndexesOfLine 3", expectedNeighbours, ((SparseMatrix)matrix).getColumnIndexesOfLine(3));
	}
	
	@Test
	public void testInitializeArrayListCLIMatrix() {
		int defaultValue = 10;
		ArrayListCLI cli = new ArrayListCLI();
		cli.addContent(0, 0, defaultValue);
		cli.addContent(0, 1, defaultValue);
		cli.addContent(0, 2, defaultValue);
		cli.addContent(0, 3, defaultValue);
		cli.addContent(1, 0, defaultValue);
		cli.addContent(1, 1, defaultValue);
		cli.initializeMatrice();
		
		assertEquals("testInitializeArrayListCLIMatrix 0 0", 0.25f, cli.getCell(0, 0), 0.0f);
		assertEquals("testInitializeArrayListCLIMatrix 0 1", 0.25f, cli.getCell(0, 1), 0.0f);
		assertEquals("testInitializeArrayListCLIMatrix 0 2", 0.25f, cli.getCell(0, 2), 0.0f);
		assertEquals("testInitializeArrayListCLIMatrix 0 3", 0.25f, cli.getCell(0, 3), 0.0f);
		assertEquals("testInitializeArrayListCLIMatrix 1 0", 0.5f, cli.getCell(1, 0), 0.0f);
		assertEquals("testInitializeArrayListCLIMatrix 1 1", 0.5f, cli.getCell(1, 1), 0.0f);
	}
}
