package tests.commons.pagewordrelation;

import commons.pagewordrelation.ArrayListPageWordsRelation;
import commons.pagewordrelation.PageWordsRelation;

public class PerformancesArrayListPageWordsRelation {
	public static void main(String[] args) {
		if(args.length != 2) {
			System.err.println("Usage is:");
			System.err.println("\tint list number");
			System.err.println("\tint page number");
			return;
		}
		int listPages = Integer.parseInt(args[0]);
		int words = Integer.parseInt(args[1]);
		
		PageWordsRelation pageWordsRelation = new ArrayListPageWordsRelation();
		new TimePerformancesPageWordsRelation(pageWordsRelation, listPages, words);
	}
}
