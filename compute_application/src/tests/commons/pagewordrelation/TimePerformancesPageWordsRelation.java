package tests.commons.pagewordrelation;

import commons.pagewordrelation.PageWordsRelation;

/*
 * Start with <br/>
 * java -Xms3G -Xmx3G tests/commons/pagewordrelation/class
 */
public class TimePerformancesPageWordsRelation {
	public TimePerformancesPageWordsRelation(
			PageWordsRelation pageWordsRelation,
			int pageNumber,
			int wordNumber) {
		long start = 0;
		long stop = 0;
		
		start = System.currentTimeMillis(); 
		fillPageWordsRelation(pageWordsRelation, pageNumber, wordNumber);
		stop = System.currentTimeMillis();
		
		printResult(pageWordsRelation, start, stop);
	}
	
	public void fillPageWordsRelation(
			PageWordsRelation pageWordsRelation,
			int pageNumber,
			int wordNumber) {
		for(int page=0; page<pageNumber; page++) {
			for(int word=0; word<wordNumber; word++) {
				pageWordsRelation.add(word+"", page);
			}
		}
	}
	
	public void printResult(PageWordsRelation pageWordsRelation, long start, long stop) {
		System.out.print("Time to fill class "+pageWordsRelation.getClass().getSimpleName()+" is ");
		System.out.print((((stop-start)%60000)/10000)+"min ");
		System.out.print((((stop-start)%10000)/1000)+"s ");
		System.out.print(((stop-start)%1000)+"ms");
		System.out.println();
	}
}
