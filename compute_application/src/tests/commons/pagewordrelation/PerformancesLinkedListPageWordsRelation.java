package tests.commons.pagewordrelation;

import commons.pagewordrelation.LinkedListPageWordsRelation;
import commons.pagewordrelation.PageWordsRelation;

public class PerformancesLinkedListPageWordsRelation {
	public static void main(String[] args) {
		if(args.length != 2) {
			System.err.println("Usage is:");
			System.err.println("\tint list number");
			System.err.println("\tint page number");
			return;
		}
		int listPages = Integer.parseInt(args[0]);
		int words = Integer.parseInt(args[1]);
		
		PageWordsRelation pageWordsRelation = new LinkedListPageWordsRelation();
		new TimePerformancesPageWordsRelation(pageWordsRelation, listPages, words);
	}
}
