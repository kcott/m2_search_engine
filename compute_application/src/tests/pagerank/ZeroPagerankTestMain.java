package tests.pagerank;

import commons.math.matrix.ArrayListCLI;
import commons.math.matrix.SparseMatrix;
import pagerank.TracePagerank;
import pagerank.zero.ZeroPagerank;

public class ZeroPagerankTestMain {
	public static void main(String[] args) {
		SparseMatrix matrix = new ArrayListCLI();
		
		matrix.addContent(0, 1, 0.5f);
		matrix.addContent(0, 3, 0.5f);
		
		matrix.addContent(1, 2, 1);

		matrix.addContent(2, 3, 1);

		matrix.addContent(3, 0, 1f/3f);
		matrix.addContent(3, 1, 1f/3f);
		matrix.addContent(3, 2, 1f/3f);
		
		TracePagerank pagerank = new ZeroPagerank();
		pagerank.trace(System.out, matrix, 0, 3);
	}
}
