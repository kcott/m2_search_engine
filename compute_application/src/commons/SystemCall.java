package commons;

import java.io.IOException;

/**
 * Simplification to system call
 */
public class SystemCall {
	/*
	 * Singleton part
	 */
	private static SystemCall instance;
	private SystemCall() {}
	/**
	 * Get the instance of SystemCall class
	 * @return SystemCall
	 */
	public static SystemCall getInstance() {
		if(instance == null)
			instance = new SystemCall();
		return instance;
	}
	/*
	 * Active part
	 */
	/**
	 * Execute a system call and wait his end<br/>
	 * Return true if there is no error
	 * @param command String
	 * @return boolean
	 */
	public boolean executeAndWait(String command) {
		Process process;
		try {
			process = Runtime.getRuntime().exec(command);
			while(true) {
				try{
					if(process.exitValue()==0)
						return true;
					else
						return false;
				} catch(Exception eRuntime) {}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
}
