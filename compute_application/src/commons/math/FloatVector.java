package commons.math;

import java.io.Serializable;

/**
 * Represent a float vector
 */
public interface FloatVector extends Serializable {
	/**
	 * Add a value at the end of the vector<br/>
	 * If the vector have reach the maximal size, increase the size
	 * @param value float
	 */
	public void add(float value);
	/**
	 * Get the value at the specified index
	 * @param index int
	 * @return float
	 */
	public float get(int index);
	/**
	 * Set the value at the specified size by parameter value
	 * @param index
	 * @param value
	 */
	public void set(int index, float value);
	/**
	 * Return the size of the vector
	 * @return int
	 */
	public int size();
	
	public boolean equals(Object obj);
	
	public void write(String filename);
	
	public void read(String filename);
}
