package commons.math;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Vector;

/**
 * Represent a float vector implemented with Vector class
 */
public class VectorFloatVector implements FloatVector {
	private static final long serialVersionUID = 1L;
	private Vector<Float> vector;
	/**
	 * Construct a vector with a specified size
	 * @param size
	 */
	public VectorFloatVector(int size) {
		vector = new Vector<Float>(size);
	}
	/**
	 * Construct a vector with a default size
	 */
	public VectorFloatVector() {
		this(0);
	}
	/**
	 * Add a value at the end of the vector<br/>
	 * If the vector have reach the maximal size, increase the size
	 * @param value float
	 */
	public void add(float value) {
		vector.addElement(value);
	}
	/**
	 * Get the value at the specified index
	 * @param index int
	 * @return float
	 */
	public float get(int index) {
		return vector.get(index);
	}
	/**
	 * Set the value at the specified size by parameter value
	 * @param index
	 * @param value
	 */
	public void set(int index, float value) {
		vector.set(index, value);
	}
	/**
	 * Return the size of the vector
	 * @return int
	 */
	public int size() {
		return vector.size();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vector == null) ? 0 : vector.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VectorFloatVector other = (VectorFloatVector) obj;
		if (vector == null) {
			if (other.vector != null)
				return false;
		} else if (!vector.equals(other.vector))
			return false;
		return true;
	}

	@Override
	public void write(String filename) {
		OutputStreamWriter osw = null;
				
		try {
			final FileOutputStream file = new FileOutputStream(filename);
			osw = new OutputStreamWriter(file, "UTF-8");
			
			osw.write(serialVersionUID + "\n");
			
			for(int i = 0; i < vector.size(); i++) {
				osw.write(vector.get(i) + " ");
			}
			
			osw.write("\n");
			osw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(osw != null) {
					osw.flush();
					osw.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void read(String filename) {
		BufferedReader br = null;
		
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

			long version = Long.parseLong(br.readLine());
			String line;
			int length;
			
			while((line = br.readLine()) != null) {
				length = line.split(" ").length;
				
				vector = new Vector<Float>(length);
				
				for(int i = 0; i < length; i++) {
					vector.set(i, Float.parseFloat(line.split(" ")[i]));
				}
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
}