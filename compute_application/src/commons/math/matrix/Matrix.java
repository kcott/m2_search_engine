package commons.math.matrix;

import java.io.Serializable;

import commons.math.FloatVector;

public interface Matrix extends Serializable{
	/**
	 * Add a content in the matrix<br/>
	 * If a content exist at the specified place, he's replace
	 * @param line int beginning by 0
	 * @param column int beginning by 0
	 * @param content float
	 */
	public void addContent(int line, int column, float content);
	/**
	 * Multiply the matrix by a vector, replace the vector given in param
	 * @param vector FloatVector
	 * @return FloatVector
	 */
	public FloatVector multiply(FloatVector vector);
	/**
	 * Multiply by the transposed matrix by a vector, replace the vector given in param
	 * @param vector FloatVector
	 * @return FloatVector
	 */
	public FloatVector multiplyByTransposed(FloatVector vector);
	/**
	 * Get the column size
	 * @return int
	 */
	public int getColumnSize();
	/**
	 * Get the line size
	 * @return int
	 */
	public int getLineSize();
	/**
	 * Get the cell value
	 * @param line int
	 * @param column int
	 * @return float
	 */
	public float getCell(int line, int column);
	public void write(String filename);
	public void read(String filename);
}
