package commons.math.matrix;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import commons.math.FloatVector;
import commons.math.VectorFloatVector;

/**
 * CLI format<br/>
 * Format to store sparce matrix
 */
public class ArrayListCLI implements CLI{
	private static final long serialVersionUID = 1L;
	/**
	 * Case content not null of a matrix
	 */
	private List<Float> contents;
	/**
	 * New line index to c
	 */
	private List<Integer> newLines;
	/**
	 * Column index to c
	 */
	private List<Integer> columnIndexes;
	/**
	 * Max column index to c
	 */
	private int maxColumnIndex;
	/**
	 * Size of the matrix
	 */
	private int n;
	/**
	 * Construct a matrix under form CLI
	 */
	public ArrayListCLI() {
		contents = new ArrayList<Float>();
		newLines = new ArrayList<Integer>();
		newLines.add(0);
		columnIndexes = new ArrayList<Integer>();
		maxColumnIndex = 0;
		n = 0;
	}
	
	/**
	 * Add a content in the matrix<br/>
	 * If a content exist at the specified place, he's replace
	 * @param line int beginning by 0
	 * @param column int beginning by 0
	 * @param content float
	 */
	public void addContent(int line, int column, float content) {
		line += 1;
		// Update max column index
		maxColumnIndex = Math.max(maxColumnIndex, column+1);
		
		// Update size of newLines list
		for (int l=newLines.size() ; l<=line ; l++) {
			newLines.add(newLines.get(l-1));
		}
		
		// Update the size of the matrix
		n = Math.max(maxColumnIndex, newLines.size()-1);
		
		// Gets index to begin scanning on columns list and indexes list
		int index = newLines.get(line-1);
		
		// Gets the number to scanning on columns list and indexes list
		int scanning = newLines.get(line) - newLines.get(line-1);
		
		// Search index where to add the content on columns list and indexes list
		boolean update = false;
		for (int s=0 ; s<scanning ; s++) {
			if (column < columnIndexes.get(index+s)) {
				contents.add(index+s, content);
				columnIndexes.add(index+s, column);
				updateNewLines(line);
				update = true;
				break;
			} else if (column == columnIndexes.get(index+s)) {
				contents.set(index+s, content);
				update = true;
				break;
			}
		}
		if (!update) {
			contents.add(index+scanning, content);
			columnIndexes.add(index+scanning, column);
			updateNewLines(line);
		}
	}
	
	/**
	 * Update the new lines table with +1 for the index and the following 
	 * @param index int
	 */
	private void updateNewLines(int index) {
		for (int i=index ; i<newLines.size() ; i++) {
			newLines.set(i, newLines.get(i) + 1);
		}
	}
	
	/**
	 * Get contents table
	 * @return float[]
	 */
	public float[] getContents() {
		Float[] tmp = new Float[contents.size()];
		contents.toArray(tmp);
		float[] tmp2 = new float[tmp.length];
		for(int f=0; f<tmp.length; f++)
			tmp2[f] = tmp[f]; 
		return tmp2;
	}
	
	/**
	 * Get new newLines table
	 * @return int[]
	 */
	public int[] getNewLines() {
		Integer[] tmp = new Integer[newLines.size()];
		newLines.toArray(tmp);
		int[] tmp2 = new int[tmp.length];
		for(int f=0; f<tmp.length; f++)
			tmp2[f] = tmp[f]; 
		return tmp2;
	}
	
	/**
	 * Get column indexes table
	 * @return int[]
	 */
	public int[] getColumnIndexes() {
		Integer[] tmp = new Integer[columnIndexes.size()];
		columnIndexes.toArray(tmp);
		int[] tmp2 = new int[tmp.length];
		for(int f=0; f<tmp.length; f++)
			tmp2[f] = tmp[f]; 
		return tmp2;
	}

	/**
	 * Multiply the matrix by a vector, replace the vector given in param
	 * @param vector FloatVector
	 * @return FloatVector
	 */
	public FloatVector multiply(FloatVector vector) {
		if (vector.size() < n)
			throw new IllegalArgumentException("vector is too small");
		// else, the rest of the matrix is considered full by 0
		int lineNumber = Math.min(n, vector.size());
		int sizeResultVector = Math.max(n, vector.size());

		int c = 0; // column
		int i = 0; // index to C
		FloatVector resultVector = new VectorFloatVector(sizeResultVector);
		int line = 0;
		float result = 0;
		for (line=0 ; line<lineNumber ; line++) {
			result = 0;
			for (int scanning=0 ; scanning<newLines.get(line+1)-newLines.get(line) ; scanning++) {
				c = columnIndexes.get(newLines.get(line)+scanning);
				i = newLines.get(line)+scanning;
				result += contents.get(i) * vector.get(c);
			}
			resultVector.add(result);
		}
		
		// Set the vector size
		for (line=lineNumber ; line<sizeResultVector ; line++)
			resultVector.add((float) 0);
		return resultVector;
	}
	
	@Override
	public String toString() {
		String str = "";
		str = "C:";
		for (Float c:contents)
			str += c +" ";
		str += "\nL:";
		for (Integer l:newLines)
			str += l +" ";
		str += "\nI:";
		for (Integer ei:columnIndexes)
			str += ei +" ";
		str += "\n";
		return str;
	}
	
	/**
	 * Multiply by the transposed matrix by a vector, replace the vector given in param
	 * @param vector FloatVector
	 * @return FloatVector
	 */
	public FloatVector multiplyByTransposed(FloatVector vector){
		if (vector.size() < n)
			throw new IllegalArgumentException("vector is too small");
		// else, the rest of the matrix is considered full by 0
		int sizeResultVector = Math.max(n, vector.size());
		
		int line = 0;
		int c = 0; // column
		int i = 0; // index to C
		float floatVector[] = new float[sizeResultVector];
		for(line=0 ; line<sizeResultVector ; line ++)
			floatVector[line] = 0;
		for (line=0 ; line<newLines.size()-1 ; line++) {
			for (int scanning=0 ; scanning<newLines.get(line+1)-newLines.get(line) ; scanning++) {
				c = columnIndexes.get(newLines.get(line)+scanning);
				i = newLines.get(line)+scanning;
				floatVector[c] = floatVector[c] + contents.get(i) * vector.get(line);
			}
		}
		line++;
		
		FloatVector resultVector = new VectorFloatVector(sizeResultVector);
		for(line=0 ; line<sizeResultVector ; line ++)
			resultVector.add(floatVector[line]);
		
		return resultVector;
	}
	/**
	 * Get the column size
	 * @return int
	 */
	public int getColumnSize() {
		return maxColumnIndex;
	}
	/**
	 * Get the line size
	 * @return int
	 */
	public int getLineSize() {
		return newLines.size()-1;
	}
	/**
	 * Get the cell value
	 * @param line int
	 * @param column int
	 * @return float
	 */
	public float getCell(int line, int column) {
		if ((getLineSize() < line) || (getColumnSize() < column))
			return 0;
		int start = newLines.get(line);
		int scanning = newLines.get(line+1) - newLines.get(line);
		for(int s=0; s<scanning; s++){
			if (columnIndexes.get(start+s) < column) {
				continue;
			} else if (columnIndexes.get(start+s) == column) {
				return contents.get(start+s);
			} else {
				break;
			}
		}
		return 0;
	}
	/**
	 * Return the size of the specified line
	 * @param line int
	 * @return int
	 */
	public int getLineSize(int line) {
		return newLines.get(line+1) - newLines.get(line);
	}
	/**
	 * Return a integer array containing column indexes to the line 
	 * @param line int
	 * @return int[]
	 */
	public int[] getColumnIndexesOfLine(int line) {
		int[] indexes = new int[getLineSize(line)];
		for (int c=0; c<getLineSize(line); c++) {
			indexes[c] = columnIndexes.get(newLines.get(line)+c);
		}
		return indexes;
	}
	
	/**
	 * Initialize the matrix with default values
	 */
	public void initializeMatrice() {
		int n = 0;
		int scan = 0;
		for (int l=1; l<newLines.size(); l++) {
			n = newLines.get(l-1);
			scan = 0;
			while((n+scan) < newLines.get(l)) {
				contents.set(
						n+scan,
						1f/(newLines.get(l)-newLines.get(l-1))
				);
				scan++;
			}
		}
	}

	public void write(String filename) {
		//ObjectOutputStream oos = null;
		OutputStreamWriter osw = null;
		
		try {
			final FileOutputStream file = new FileOutputStream(filename);
			osw = new OutputStreamWriter(file, "UTF-8");
			osw.write(serialVersionUID + "\n");
			osw.write("C " + contents.size() + " ");
			
			for(int i = 0; i < contents.size(); i++) {
				osw.write(contents.get(i) + " ");
				osw.flush();
			}
			
			osw.write("\n");
			osw.write("L " + newLines.size() + " ");
			
			for(int i = 0; i < newLines.size(); i++) {
				osw.write(newLines.get(i) + " ");
				osw.flush();
			}
			
			osw.write("\n");
			osw.write("I " + columnIndexes.size() + " ");
			
			for(int i = 0; i < columnIndexes.size(); i++) {
				osw.write(columnIndexes.get(i) + " ");
				osw.flush();
			}			
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(osw != null) {
					osw.flush();
					osw.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void read(String filename) {
		BufferedReader br = null;
		
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
			
			long version = Long.parseLong(br.readLine());
			String line;
			int length, nbLine = 1;
			
			while((line = br.readLine()) != null) {
				length = Integer.parseInt(line.split(" ")[1]);
				
				if(nbLine == 1) {
					contents = new ArrayList<Float>(length);
					
					for(int i = 0; i < length; i++) {
						contents.set(i, Float.parseFloat(line.split(" ")[i + 2]));
					}
				} else if(nbLine == 2) {	
					newLines = new ArrayList<Integer>(length);

					for(int i = 0; i < length; i++) {
						newLines.set(i, Integer.parseInt(line.split(" ")[i + 2]));
					}
				} else {
					columnIndexes = new ArrayList<Integer>(length);
					
					for(int i = 0; i < length; i++) {
						columnIndexes.set(i, Integer.parseInt(line.split(" ")[i + 2]));
					}
				}
				
				nbLine++;
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
}