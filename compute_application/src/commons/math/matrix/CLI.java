package commons.math.matrix;

/**
 * Standardized format to store sparse matrix
 */
public interface CLI extends SparseMatrix{
	/**
	 * Initialize the matrix with default values
	 */
	public void initializeMatrice();
}
