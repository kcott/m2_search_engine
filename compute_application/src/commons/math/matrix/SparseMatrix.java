package commons.math.matrix;

/**
 * Matrix containing full 0
 */
public interface SparseMatrix extends Matrix{
	/**
	 * Return the size of the specified line
	 * @param line int
	 * @return int
	 */
	public int getLineSize(int line);
	/**
	 * Return a integer array containing column indexes to the line 
	 * @param line int
	 * @return int[]
	 */
	public int[] getColumnIndexesOfLine(int line);
}
