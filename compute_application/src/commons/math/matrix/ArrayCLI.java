package commons.math.matrix;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;

import commons.math.FloatVector;
import commons.math.VectorFloatVector;

public class ArrayCLI implements CLI{
	private static final long serialVersionUID = 1L;
	/**
	 * Case content not null of a matrix
	 */
	private float[] contents;
	/**
	 * Index of last insertion in contents array
	 */
	private int indexToContents;
	/**
	 * New line index to c
	 */
	private int[] newLines;
	/**
	 * Column index to c
	 */
	private int[] columnIndexes;
	/**
	 * Max column index to c
	 */
	private int maxColumnIndex;
	/**
	 * Size of the matrix
	 */
	private int n;

	/**
	 * Construct a matrix under form CLI
	 * @param sizeOfContents int
	 * @param n int
	 */
	public ArrayCLI(int sizeOfC, int n) {
		this.contents = new float[sizeOfC];
		this.indexToContents = 0;
		this.newLines = new int[n+1];
		this.columnIndexes = new int[sizeOfC];
	}
	/**
	 * Construct a matrix under form CLI
	 * @param contents float[]
	 * @param newLines int[]
	 * @param columnIndexes int[]
	 */
	public ArrayCLI(float[] contents, int[] newLines, int[] columnIndexes) {
		this.contents = contents;
		this.indexToContents = this.contents.length;
		this.newLines = newLines;
		this.columnIndexes = columnIndexes;
	}
	/**
	 * Add a content in the matrix<br/>
	 * If a content exist at the specified place, he's replace
	 * @param line int beginning by 0
	 * @param column int beginning by 0
	 * @param content float
	 */
	public void addContent(int line, int column, float content) {
		line += 1;
		// Update max column index
		maxColumnIndex = Math.max(maxColumnIndex, column+1);
		
		// Update the size of the matrix
		n = Math.max(maxColumnIndex, newLines.length-1);
		
		// Gets index to begin scanning on columns list and indexes list
		int index = newLines[line-1];
		
		// Gets the number to scanning on columns list and indexes list
		int scanning = newLines[line] - newLines[line-1];
		
		// Search index where to add the content on columns list and indexes list
		boolean update = false;
		for (int s=0 ; s<scanning ; s++) {
			if (column < columnIndexes[index+s]) {
				shiftToTheEndContent(index+s);
				contents[index+s] = content;
				columnIndexes[index+s] = column;
				updateNewLines(line);
				indexToContents++;
				update = true;
				break;
			} else if (column == columnIndexes[index+s]) {
				contents[index+s] = content;
				indexToContents++;
				update = true;
				break;
			}
		}
		if (!update) {
			shiftToTheEndContent(index+scanning);
			contents[index+scanning] = content;
			columnIndexes[index+scanning] = column;
			indexToContents++;
			updateNewLines(line);
		}
	}
	/**
	 * Shift elements in C and I to the end of arrays
	 * @param indexOfContent int
	 */
	private void shiftToTheEndContent(int indexOfContent) {
		for (int i=indexToContents ; indexOfContent<i; i--) {
			contents[i] = contents[i-1];
			columnIndexes[i] = columnIndexes[i-1];
		}
	}
	/**
	 * Update the new lines table with +1 for the index and the following 
	 * @param index int
	 */
	private void updateNewLines(int index) {
		for (int i=index ; i<newLines.length ; i++) {
			newLines[i] = newLines[i] + 1;
		}
	}
	/**
	 * Get contents table
	 * @return float[]
	 */
	public float[] getContents() {
		return contents;
	}
	/**
	 * Get new newLines table
	 * @return int[]
	 */
	public int[] getNewLines() {
		return newLines;
	}
	
	/**
	 * Get column indexes table
	 * @return int[]
	 */
	public int[] getColumnIndexes() {
		return columnIndexes;
	}
	/**
	 * Multiply the matrix by a vector, replace the vector given in param
	 * @param vector FloatVector
	 * @return FloatVector
	 */
	public FloatVector multiply(FloatVector vector) {
		if (vector.size() < newLines.length-1)
			throw new IllegalArgumentException("vector is too small");
		// else, the rest of the matrix is considered full by 0
		int lineNumber = Math.min(newLines.length-1, vector.size());
		int sizeResultVector = Math.max(newLines.length-1, vector.size());
		
		FloatVector resultVector = new VectorFloatVector(sizeResultVector);
		int line = 0;
		for (line=0 ; line<lineNumber ; line++) {
			float result = 0;
			for (int scanning=0 ; scanning<newLines[line+1]-newLines[line] ; scanning++) {
				result += contents[newLines[line]+scanning]
						* vector.get(columnIndexes[newLines[line]+scanning]);
			}
			resultVector.add(result);
		}
		for (line=lineNumber ; line<sizeResultVector ; line++)
			resultVector.add((float) 0);
		return resultVector;
	}
	/**
	 * Multiply by the transposed matrix by a vector, replace the vector given in param
	 * @param vector FloatVector
	 * @return FloatVector
	 */
	public FloatVector multiplyByTransposed(FloatVector vector){
		if (vector.size() < n)
			throw new IllegalArgumentException("vector is too small");
		// else, the rest of the matrix is considered full by 0
		int sizeResultVector = Math.max(n, vector.size());
		
		int line = 0;
		int c = 0; // column
		int i = 0; // index to C
		float floatVector[] = new float[sizeResultVector];
		for(line=0 ; line<sizeResultVector ; line ++)
			floatVector[line] = 0;
		for (line=0 ; line<newLines.length-1 ; line++) {
			for (int scanning=0 ; scanning<newLines[line+1]-newLines[line] ; scanning++) {
				c = columnIndexes[newLines[line]+scanning];
				i = newLines[line]+scanning;
				floatVector[c] = floatVector[c] + contents[i] * vector.get(line);
			}
		}
		line++;
		
		FloatVector resultVector = new VectorFloatVector(sizeResultVector);
		for(line=0 ; line<sizeResultVector ; line ++)
			resultVector.add(floatVector[line]);
		
		return resultVector;
	}
	/**
	 * Get the column size
	 * @return int
	 */
	public int getColumnSize() {
		return maxColumnIndex;
	}
	/**
	 * Get the line size
	 * @return int
	 */
	public int getLineSize() {
		return newLines.length-1;
	}
	/**
	 * Get the cell value
	 * @param line int
	 * @param column int
	 * @return float
	 */
	public float getCell(int line, int column) {
		if ((getLineSize() < line) || (getColumnSize() < column))
			return 0;
		int start = newLines[line];
		int scanning = newLines[line+1] - newLines[line];
		for(int s=0; s<scanning; s++){
			if (columnIndexes[start+s] < column) {
				continue;
			} else if (columnIndexes[start+s] == column) {
				return contents[start+s];
			} else {
				break;
			}
		}
		return 0;
	}
	/**
	 * Return the size of the specified line
	 * @param line int
	 * @return int
	 */
	public int getLineSize(int line) {
		return newLines[line+1] - newLines[line];
	}
	/**
	 * Return a integer array containing column indexes to the line 
	 * @param line int
	 * @return int[]
	 */
	public int[] getColumnIndexesOfLine(int line) {
		int[] indexes = new int[getLineSize(line)];
		for (int c=0; c<getLineSize(line); c++) {
			indexes[c] = columnIndexes[newLines[line]+c];
		}
		return indexes;
	}
	/**
	 * Initialize the matrix with default values
	 */
	public void initializeMatrice() {
		int n = 0;
		int scan = 0;
		for (int l=1; l<newLines.length; l++) {
			n = newLines[l-1];
			scan = 0;
			while((n+scan) < newLines[l]) {
				contents[n+scan] = 1f/(newLines[l]-newLines[l-1]);
				scan++;
			}
		}
	}
	
	public void write(String filename) {
		ObjectOutputStream oos = null;

		try {
			final FileOutputStream file = new FileOutputStream(filename);
			oos = new ObjectOutputStream(file);

			oos.writeLong(serialVersionUID);
			oos.writeUTF("\n");

			oos.writeUTF("C ");
			oos.write(contents.length);
			oos.writeUTF(" ");

			for(int i = 0; i < contents.length; i++) {
				oos.writeFloat(contents[i]);
				oos.writeUTF(" ");
			}			

			oos.writeUTF("\n");
			oos.writeUTF("L ");
			oos.write(newLines.length);
			oos.writeUTF(" ");

			for(int i = 0; i < newLines.length; i++) {
				oos.writeObject(newLines[i]);
				oos.writeUTF(" ");
			}

			oos.writeUTF("\n");
			oos.writeUTF("I ");
			oos.write(columnIndexes.length);
			oos.writeUTF(" ");

			for(int i = 0; i < columnIndexes.length; i++) {
				oos.writeObject(columnIndexes[i]);
				oos.writeUTF(" ");
			}

			oos.writeUTF("\n");
			oos.flush();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(oos != null) {
					oos.flush();
					oos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void read(String filename) {
BufferedReader br = null;
		
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
			
			long version = Long.parseLong(br.readLine());
			String line;
			int length, nbLine = 1;
			
			while((line = br.readLine()) != null) {
				length = Integer.parseInt(line.split(" ")[1]);
				
				if(nbLine == 1) {
					contents = new float[length];
					
					for(int i = 0; i < length; i++) {
						contents[i] = Float.parseFloat(line.split(" ")[i + 2]);
					}
				} else if(nbLine == 2) {	
					newLines = new int[length];

					for(int i = 0; i < length; i++) {
						newLines[i] = Integer.parseInt(line.split(" ")[i + 2]);
					}
				} else {
					columnIndexes = new int[length];
					
					for(int i = 0; i < length; i++) {
						columnIndexes[i] = Integer.parseInt(line.split(" ")[i + 2]);
					}
				}
				
				nbLine++;
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
}
