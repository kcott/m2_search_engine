package commons.dico;

import java.io.Serializable;

public interface EmptyWords extends Serializable {
	/**
	 * Get the number of empty words
	 * @return int number of empty words
	 */
	public int size();
	/**
	 * Get the empty word to the parameter index
	 * @param index int
	 * @return String empty word
	 */
	public String get(int index);
	/**
	 * Set the empty word to the parameter index
	 * @param index int
	 * @param word String
	 */
	public void set(int index, String word);
	/**
	 * Return true if the word is containing in the class
	 * @param word String
	 * @return boolean
	 */
	public boolean contains(String word);
	/**
	 * Load a file with empty words on each line in memory
	 * @param filename String
	 */
	public void read(String filename);
}
