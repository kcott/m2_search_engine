package commons.dico;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ArrayListEmptyWords implements EmptyWords{
	private static final long serialVersionUID = 1L;
	List<String> emptyWords;
	/**
	 * Construct a empty words array with the size n
	 * @param n
	 */
	public ArrayListEmptyWords() {
		emptyWords = new ArrayList<String>();
	}
	/**
	 * Get the number of empty words
	 * @return int number of empty words
	 */
	public int size() {
		return emptyWords.size();
	}
	/**
	 * Get the empty word to the parameter index
	 * @param index int
	 * @return String empty word
	 */
	public String get(int index) {
		return emptyWords.get(index);
	}
	/**
	 * Set the empty word to the parameter index
	 * @param index int
	 * @param word String
	 */
	public void set(int index, String word) {
		emptyWords.set(index, word);
	}
	/**
	 * Return true if the word is containing in the class
	 * @param word String
	 * @return boolean
	 */
	public boolean contains(String word) {
		return emptyWords.contains(word);
	}
	/**
	 * Load a file with empty words on each line in memory
	 * @param filename String
	 */
	public void read(String filename) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(
					new InputStreamReader(
							new FileInputStream(filename), 
							"UTF-8"
							)
			);

			String line;
			while((line = reader.readLine()) != null){
				emptyWords.add(line.split(" ")[0]);
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {}
		}
	}
}
