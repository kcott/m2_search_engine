package commons.pagewordrelation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

public class SimpleIndexToWord implements IndexToWord {
	private static final long serialVersionUID = 1L;
	private Map<Integer, String> indexToWord;

	public SimpleIndexToWord() {
		indexToWord = new HashMap<Integer, String>();
	}
	
	@Override
	public void set(int index, String word) {
		indexToWord.put(index, word);
	}

	@Override
	public String get(int index) {
		return indexToWord.get(index);
	}

	@Override
	public void write(String filename) {
		ObjectOutputStream oos = null;
		
		try {
			final FileOutputStream file = new FileOutputStream(filename);
			oos = new ObjectOutputStream(file);
			oos.writeObject(indexToWord);
			oos.flush();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(oos != null) {
					oos.flush();
					oos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public void read(String filename) {
		ObjectInputStream ois = null;
		try {
			final FileInputStream file = new FileInputStream(filename);
			ois = new ObjectInputStream(file);
			indexToWord = (HashMap<Integer, String>)ois.readObject();
		} catch(IOException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(ois != null) {
					ois.close();
				}
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}

}