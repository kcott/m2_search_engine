package commons.pagewordrelation;

import java.io.Serializable;

public interface PageToIndex extends Serializable {
	public void set(String page, int index);
	public Integer get(String page);
	public void write(String filename);
	public void read(String filename);
}
