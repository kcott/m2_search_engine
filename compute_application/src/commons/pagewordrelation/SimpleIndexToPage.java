package commons.pagewordrelation;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class SimpleIndexToPage implements IndexToPage {
	private static final long serialVersionUID = 1L;
	private Map<Integer, String> indexToPage;

	public SimpleIndexToPage() {
		indexToPage = new HashMap<Integer, String>(); 
	}

	public void set(int index, String page) {
		indexToPage.put(index, page);
	}

	public String get(int index) {
		return indexToPage.get(index);
	}
	
	public void write(String filename) {
		OutputStreamWriter osw = null;
		
		try {
			final FileOutputStream file = new FileOutputStream(filename);
			osw = new OutputStreamWriter(file, "UTF-8");
			osw.write("" + serialVersionUID + "\n");
			for(Entry<Integer,String> entry: indexToPage.entrySet()) {
				osw.write(entry.getKey() + " " + entry.getValue() + "\n");
				osw.flush();
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(osw != null) {
					osw.flush();
					osw.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void read(String filename) {
		indexToPage = new HashMap<Integer,String>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(
					new InputStreamReader(
							new FileInputStream(filename), 
							"UTF-8"
							)
			);

			long version = Long.parseLong(reader.readLine());
			String line;

			while((line = reader.readLine()) != null){
				indexToPage.put(
						Integer.parseInt(line.split(" ")[0]),
						line.split(" ")[1]
				);
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {}
		}
	}
}