package commons.pagewordrelation;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class SimplePageToIndex implements PageToIndex {
	private static final long serialVersionUID = 1L;
	private Map<String, Integer> pageToIndex;

	public SimplePageToIndex() {
		pageToIndex = new HashMap<String, Integer>(); 
	}

	public void set(String page, int index) {
		pageToIndex.put(page, index);
	}

	public Integer get(String page) {
		return pageToIndex.get(page);
	}
	
	public void write(String filename) {
		OutputStreamWriter osw = null;
		
		try {
			final FileOutputStream file = new FileOutputStream(filename);

			osw = new OutputStreamWriter(file, "UTF-8");
			osw.write("" + serialVersionUID + "\n");
			for(Entry<String,Integer> entry: pageToIndex.entrySet()) {
				osw.write(entry.getKey() + " " + entry.getValue() + "\n");
				osw.flush();
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(osw != null) {
					osw.flush();
					osw.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void read(String filename) {
		pageToIndex = new HashMap<String,Integer>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(
					new InputStreamReader(
							new FileInputStream(filename), 
							"UTF-8"
							)
			);

			long version = Long.parseLong(reader.readLine());
			String line;
			while((line = reader.readLine()) != null){					       
				pageToIndex.put(
						line.split(" ")[0],
						Integer.parseInt(line.split(" ")[1])
				);
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {}
		}
	}
}