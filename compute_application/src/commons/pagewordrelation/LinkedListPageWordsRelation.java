package commons.pagewordrelation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Map;

public class LinkedListPageWordsRelation implements PageWordsRelation{
	private static final long serialVersionUID = 1L;
	private Map<String, LinkedList<Integer>> pageWords;

	public LinkedListPageWordsRelation() {
		pageWords = new HashMap<String, LinkedList<Integer>>();
	}

	public void add(String word, Integer page) {
		if (!pageWords.containsKey(word))
			pageWords.put(word, new LinkedList<Integer>());
		pageWords.get(word).add(page);
	}

	public Integer[] getPages(String word) {
		Integer[] pages = new Integer[pageWords.get(word).size()];
		LinkedList<Integer> pagesTmp = pageWords.get(word);
		for(int i = 0; i < pagesTmp.size() ; i++)
			pages[i] = pagesTmp.get(i);
		return pages;
	}
	
	public int getPageNumberForWord(String word) {
		return pageWords.get(word).size();
	}
	
	public Integer getPage(String word, Integer index) {
		return pageWords.get(word).get(index);
	}

	public void write(String filename) {
		ObjectOutputStream oos = null;
		
		try {
			final FileOutputStream file = new FileOutputStream(filename);
			oos = new ObjectOutputStream(file);
			oos.writeObject(pageWords);
			oos.flush();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(oos != null) {
					oos.flush();
					oos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void read(String filename) {
		ObjectInputStream ois = null;
		
		try {
			final FileInputStream file = new FileInputStream(filename);
			ois = new ObjectInputStream(file);
			pageWords = (HashMap<String, LinkedList<Integer>>)ois.readObject();
		} catch(IOException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(ois != null) {
					ois.close();
				}
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
}