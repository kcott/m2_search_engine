package commons.pagewordrelation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

public class HashMapPageWordsRelation implements PageWordsRelation{
	private static final long serialVersionUID = 1L;
	private Map<String, HashMap<Integer, Integer>> pageWords;

	public HashMapPageWordsRelation() {
		pageWords = new HashMap<String,  HashMap<Integer, Integer>>();
	}

	public void add(String word, Integer page) {
		if (!pageWords.containsKey(word))
			pageWords.put(word, new HashMap<Integer, Integer>());
		pageWords.get(word).put(page, 0);
	}

	public Integer[] getPages(String word) {
		Integer[] pages = new Integer[pageWords.get(word).size()];
		HashMap<Integer, Integer> pagesTmp = pageWords.get(word);
		for(int i = 0; i < pagesTmp.size() ; i++)
			pages[i] = pagesTmp.get(i);
		return pages;
	}

	public int getPageNumberForWord(String word) {
		return pageWords.get(word).size();
	}
	
	public Integer getPage(String word, Integer index) {
		return pageWords.get(word).get(index);
	}
	
	public void write(String filename) {
		ObjectOutputStream oos = null;
		
		try {
			final FileOutputStream file = new FileOutputStream(filename);
			oos = new ObjectOutputStream(file);
			oos.writeObject(pageWords);
			oos.flush();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(oos != null) {
					oos.flush();
					oos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void read(String filename) {
		ObjectInputStream ois = null;
		
		try {
			final FileInputStream file = new FileInputStream(filename);
			ois = new ObjectInputStream(file);
			pageWords = (HashMap<String, HashMap<Integer, Integer>>)ois.readObject();
		} catch(IOException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(ois != null) {
					ois.close();
				}
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
}