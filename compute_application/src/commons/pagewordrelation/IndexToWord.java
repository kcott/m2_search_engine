package commons.pagewordrelation;

import java.io.Serializable;

public interface IndexToWord extends Serializable{
	public void set(int index, String word);
	public String get(int index);
	public void write(String filename);
	public void read(String filename);
}
