package commons.pagewordrelation;

import java.io.Serializable;

public interface IndexToPage extends Serializable {
	public void set(int index, String page);
	public String get(int index);
	public void write(String filename);
	public void read(String filename);
}
