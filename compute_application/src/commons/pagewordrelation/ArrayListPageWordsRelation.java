package commons.pagewordrelation;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ArrayListPageWordsRelation implements PageWordsRelation{
	private static final long serialVersionUID = 1L;
	private Map<String, ArrayList<Integer>> pageWords;

	public ArrayListPageWordsRelation() {
		pageWords = new HashMap<String, ArrayList<Integer>>();
	}

	public void add(String word, Integer page) {
		if (!pageWords.containsKey(word))
			pageWords.put(word, new ArrayList<Integer>());
		pageWords.get(word).add(page);
	}

	public Integer[] getPages(String word) {
		Integer[] pages = new Integer[pageWords.get(word).size()];
		ArrayList<Integer> pagesTmp = pageWords.get(word);
		for(int i = 0; i < pagesTmp.size() ; i++)
			pages[i] = pagesTmp.get(i);
		return pages;
	}

	public int getPageNumberForWord(String word) {
		return pageWords.get(word).size();
	}
	
	public Integer getPage(String word, Integer index) {
		return pageWords.get(word).get(index);
	}
	
	public void write(String filename) {
		//ObjectOutputStream oos = null;
		OutputStreamWriter osw = null;
		
		try {
			final FileOutputStream file = new FileOutputStream(filename);
			osw = new OutputStreamWriter(file, "UTF-8");
			osw.write("" + serialVersionUID + "\n");
			
			for(Entry<String, ArrayList<Integer>> entry: pageWords.entrySet()) {
				osw.write(entry.getKey() + " " /*+ entry.getValue() + "\n"*/);
				for(Integer id: entry.getValue()) {
					osw.write(id + " ");
				}
				osw.write("\n");
				osw.flush();
			}		
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(osw != null) {
					osw.flush();
					osw.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}		
		}
	}
	
	@SuppressWarnings("unchecked")
	public void read(String filename) {
		//ObjectInputStream ois = null;
		pageWords = new HashMap<String, ArrayList<Integer>>();
		BufferedReader reader = null;

		try {
			final FileInputStream file = new FileInputStream(filename);
			reader = new BufferedReader(new InputStreamReader(file, "UTF-8"));
			long version = Long.parseLong(reader.readLine());
			String line;
			int length;
			ArrayList<Integer> value = null;
			
			while((line = reader.readLine()) != null){
				length = line.split(" ").length;
				value = new ArrayList<Integer>(length);
				
				for(int i = 1; i < length; i++) {
					value.add(Integer.parseInt(line.split(" ")[i]));
				}
				
				pageWords.put(line.split(" ")[0], value);
			}

		//	ois = new ObjectInputStream(file);
			//pageWords = (HashMap<String, ArrayList<Integer>>)ois.readObject();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {}
		}
	}
}