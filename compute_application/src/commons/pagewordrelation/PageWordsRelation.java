package commons.pagewordrelation;

import java.io.Serializable;

public interface PageWordsRelation extends Serializable{
	/**
	 * Add relation between a word and a page
	 * @param word String
	 * @param page Integer
	 */
	public void add(String word, Integer page);
	/**
	 * Get all pages for the parameter word
	 * @param word String
	 * @return Integer[]
	 */
	public Integer[] getPages(String word);
	/**
	 * Get the page number for the parameter word
	 * @param word String
	 * @return int
	 */
	public int getPageNumberForWord(String word);
	/**
	 * Get the page for a parameter word to the parameter index
	 * @param word String
	 * @param index Integer
	 * @return Integer
	 */
	public Integer getPage(String word, Integer index);
	/**
	 * Serializing the class in the parameter filename
	 * @param filename String
	 */
	public void write(String filename);
	/**
	 * Unserializing the class in the parameter filename
	 * @param filename String
	 */
	public void read(String filename);
}
