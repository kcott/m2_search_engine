package pagerank.zero;

import pagerank.PagerankConfiguration;
import system.ReaderConfiguration;
import system.SystemConfiguration;

public class ZeroPagerankConfiguration implements PagerankConfiguration{
	private final String FILE = "zeropagerank.conf";
	/*
	 * Singleton
	 */
	private static ZeroPagerankConfiguration instance;
	public static ZeroPagerankConfiguration getInstance(){
		if(instance == null)
			instance = new ZeroPagerankConfiguration();
		return instance;
	}
	/*
	 * Real class
	 */
	ReaderConfiguration configuration;
	private ZeroPagerankConfiguration(){
		String directoryConf = SystemConfiguration.getInstance().getConfsDirectory();
		configuration = new ReaderConfiguration(directoryConf + FILE);
	}
	/**
	 * Return the difference to sum of vector to each step of computing<br/>
	 * to considering the end of the algorithm
	 * @return float
	 */
	public float getEpsilon() {
		return Float.parseFloat(configuration.getConf("epsilon"));
	}
}