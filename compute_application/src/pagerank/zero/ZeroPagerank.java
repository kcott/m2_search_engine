package pagerank.zero;

import java.io.PrintStream;

import commons.math.FloatVector;
import commons.math.VectorFloatVector;
import commons.math.matrix.SparseMatrix;
import pagerank.PagerankAlgorithm;
import pagerank.TracePagerank;

public class ZeroPagerank extends PagerankAlgorithm implements TracePagerank{
	/**
	 * Construct an zero page rank algorithm
	 * @param epsilon float
	 */
	public ZeroPagerank(float epsilon){
		setEpsilon(epsilon);
	}
	/**
	 * Construct an zero page rank algorithm with a default epsilon
	 */
	public ZeroPagerank(){
		setEpsilon(ZeroPagerankConfiguration.getInstance().getEpsilon());
	}
	/**
	 * Compute the page ranks of a given matrix 
	 * @param matrix SparseMatrix
	 * @return FloatVector
	 */
	public FloatVector pagerank(SparseMatrix matrix){
		FloatVector inputVector = initialVector(matrix);
		FloatVector outputVector = null;
		
		// Compute page rank until stable version
		float delta = 0;
		while(true){
			outputVector = stepCompute(matrix, inputVector);
			delta = 0;
			for(int line=0; line < inputVector.size(); line++)
				delta += Math.abs(inputVector.get(line) - outputVector.get(line));
			if(delta < getEpsilon())
				break;
			inputVector = outputVector;
		}
		return outputVector;
	}
	/**
	 * Compute a step of zero page rank algorithm 
	 * @param matrix SparseMatrix
	 * @param inputVector FloatVector
	 * @return FloatVector
	 */
	private FloatVector stepCompute(SparseMatrix matrix, FloatVector inputVector){
		return matrix.multiplyByTransposed(inputVector);
	}
	/**
	 * Initiate vector to first compute
	 * @param matrix Matrix
	 * @param size int
	 */
	private FloatVector initialVector(SparseMatrix matrix){
		int size = Math.max(matrix.getLineSize(), matrix.getColumnSize());
		FloatVector vector = new VectorFloatVector();
		for(int line=0; line < size; line++){
			vector.add((float) (1./size));
		}
		return vector;
	}
	/**
	 * Print in out, the probability to be on each neighbor vertex
	 * @param out PrintWriter
	 * @param matrix SparseMatrix
	 * @param vertex int
	 * @param step int
	 */
	public void trace(PrintStream out, SparseMatrix matrix, int vertex, int step){
		FloatVector inputVector = initialVector(matrix);
		FloatVector outputVector = null;
		
		// Get neighbors of vertex
		int[] indexNeighbors = matrix.getColumnIndexesOfLine(vertex);
		
		// Trace initial state
		out.print("Step 0:");
		for(int i=0; i<indexNeighbors.length; i++) { // index on array
			out.print(inputVector.get(i) + " ");
		}
		out.println();
		
		// Trace to n step
		for (int s=0; s<step; s++){
			outputVector = stepCompute(matrix, inputVector);
			inputVector = outputVector;
			
			// Trace step
			out.print("Step "+(s+1)+":");
			for(int i=0; i<indexNeighbors.length; i++) { // index on array
				out.print(inputVector.get(i) + " ");
			}
			out.println();
		}
	}
}
