package pagerank.zero;

import commons.math.FloatVector;
import commons.math.VectorFloatVector;
import commons.math.matrix.ArrayListCLI;
import commons.math.matrix.CLI;

public class MainZeroPageRank {

	public static void main(String[] args) {
		CLI cli = new ArrayListCLI();
		ZeroPagerank zero = new ZeroPagerank(0.0001f);
		FloatVector vector = new VectorFloatVector();
		
		cli.read("cli.ser");
		vector = zero.pagerank(cli);
		vector.write("zero.ser");
	}

}
