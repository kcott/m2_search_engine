package pagerank;

public interface PagerankConfiguration {
	/**
	 * Return the difference to sum of vector to each step of computing<br/>
	 * to considering the end of the algorithm
	 * @return float
	 */
	public float getEpsilon();
}
