package pagerank;

import commons.math.FloatVector;
import commons.math.matrix.SparseMatrix;

public abstract class PagerankAlgorithm {
	/**
	 * Value for different of two vectors to stop algorithm
	 */
	private float epsilon;
	/**
	 * Compute the page ranks of a given matrix 
	 * @param matrix SparseMatrix
	 * @return FloatVector
	 */
	public abstract FloatVector pagerank(SparseMatrix matrix);
	/**
	 * Get value for different of two vectors to stop algorithm
	 * @return float
	 */
	public float getEpsilon() {
		return epsilon;
	}
	/**
	 * Set value for different of two vectors to stop algorithm
	 * @param epsilon float
	 */
	public void setEpsilon(float epsilon) {
		this.epsilon = epsilon;
	}
}
