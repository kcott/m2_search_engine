package pagerank.zap;

import commons.math.FloatVector;
import commons.math.VectorFloatVector;
import commons.math.matrix.ArrayListCLI;
import commons.math.matrix.CLI;

public class MainPageRankZap {

	public static void main(String[] args) {
		CLI cli = new ArrayListCLI();
		ZapPagerank zap = new ZapPagerank(0.0001f, 0.1f);
		FloatVector vector = new VectorFloatVector();
		
		cli.read("cli.ser");
		vector = zap.pagerank(cli);
		vector.write("zap.ser");
	}

}
