package pagerank.zap;

import pagerank.PagerankConfiguration;
import system.ReaderConfiguration;
import system.SystemConfiguration;

public class ZapPagerankConfiguration implements PagerankConfiguration{
	private final String FILE = "zappagerank.conf";
	/*
	 * Singleton
	 */
	private static ZapPagerankConfiguration instance;
	public static ZapPagerankConfiguration getInstance(){
		if(instance == null)
			instance = new ZapPagerankConfiguration();
		return instance;
	}
	/*
	 * Real class
	 */
	ReaderConfiguration configuration;
	private ZapPagerankConfiguration(){
		String directoryConf = SystemConfiguration.getInstance().getConfsDirectory();
		configuration = new ReaderConfiguration(directoryConf + FILE);
	}
	/**
	 * Return the difference to sum of vector to each step of computing<br/>
	 * to considering the end of the algorithm
	 * @return float
	 */
	public float getEpsilon() {
		return Float.parseFloat(configuration.getConf("epsilon"));
	}
	/**
	 * Return the probability to zap to an another page
	 * @return float
	 */
	public float getD() {
		return Float.parseFloat(configuration.getConf("d"));
	}
}
