package pagerank;

import java.io.PrintStream;

import commons.math.matrix.SparseMatrix;

public interface TracePagerank{
	/**
	 * Print in out, the probability to be on each neighbor vertex
	 * @param out PrintWriter
	 * @param matrix SparseMatrix
	 * @param vertex int
	 * @param step int
	 */
	public void trace(PrintStream out, SparseMatrix matrix, int vertex, int step);
}
