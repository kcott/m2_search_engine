package webcrawler.cliLoading;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import webcrawler.Parser;
import webcrawler.WikipediaHandler;
import webcrawler.createLink.InternalLink;
import webcrawler.createLink.InternalRef;

import commons.math.matrix.CLI;
import commons.pagewordrelation.PageToIndex;

public class WikipediaCLIHandler extends WikipediaHandler {

    private boolean bText = false, bTitle = false;
    private String contenu, title, link = LINK;
    private StringBuffer sb;
    private Parser parser;
    private Integer id;
    
    CLI cli = null;
    PageToIndex pageToIndex = null;
    
    public WikipediaCLIHandler(CLI cli, PageToIndex pageToIndex) {
    	this.cli = cli;
    	this.pageToIndex = pageToIndex;
    }
    
    /**
     * 
     */
    @Override
    public void startElement(String uri, String localName, String qName,
    		Attributes attributes) throws SAXException {
    	if(CMP_TEXT.equals(qName)) {
    		bText = true;
    		sb = new StringBuffer();
    	} else if(CMP_TITLE.equals(qName)) {
    		bTitle = true;
    		sb = new StringBuffer();
    	}
    }
    
    @Override
    public void endElement(String uri, String localName, String qName) {
    	if(CMP_TEXT.equals(qName)) {
    		link += title.replaceAll(" ", "_");
    		contenu = null;
    		contenu = sb.toString();

    		//Get the id from this page
    		id = pageToIndex.get(link);
    		
    		//Create a new parser which will fill the CLI by calling
    		//the computeLink method which will find all internal link
    		//in a page
    		if(id != null) {
    			parser = new Parser(new InternalLink());
    			parser.computeLink(contenu, id, pageToIndex, cli);
    			parser.setLinkStrategy(new InternalRef());
    			parser.computeLink(contenu, id, pageToIndex, cli);
    		}

    		link = LINK;
    		bText = false;
    	} else if(CMP_TITLE.equals(qName)) {
    		title = sb.toString();
    		bTitle = false;
    	}
    }
    
    /**
     * 
     */
    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
    	if(bText) {
    		sb.append(new String(ch, start, length));
      	} else if(bTitle) {
    		sb.append(new String(ch, start, length));
    	} 
    }
}