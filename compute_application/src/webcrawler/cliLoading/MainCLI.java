package webcrawler.cliLoading;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import commons.math.matrix.ArrayListCLI;
import commons.math.matrix.CLI;
import commons.pagewordrelation.PageToIndex;
import commons.pagewordrelation.SimplePageToIndex;

public class MainCLI {
	public static void main(String[] args) {		
		if (args.length != 1) {
			System.err.println("waiting xml filepath to parse");
			return;
		}

		System.out.println("Start parsing");
		long start = System.nanoTime();
		
		CLI cli = new ArrayListCLI();

		SAXParserFactory factory = SAXParserFactory.newInstance();
		
		try {
			InputStream inputXML = new FileInputStream(args[0]);
			SAXParser parser = factory.newSAXParser();
			PageToIndex pageToIndex = new SimplePageToIndex();
			
			pageToIndex.read("pageToIndex.ser");
			
			WikipediaCLIHandler handler = 
					new WikipediaCLIHandler(cli, pageToIndex);
			
			parser.parse(inputXML, handler);
			cli.initializeMatrice();
			cli.write("cli.ser");
		} catch (Throwable e) {
			e.printStackTrace();
		}

		long end = System.nanoTime();
		System.out.println("total : " + ((end - start) / 1000000) + "ms");
		System.out.println("End parsing");
	}
}