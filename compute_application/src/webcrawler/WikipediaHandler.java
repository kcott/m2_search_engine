package webcrawler;

public abstract class WikipediaHandler extends Handler {
    protected final String LINK = "https://fr.wikipedia.org/wiki/";
    
    protected final String CMP_TEXT = "text";
    protected final String CMP_PAGE = "page";
    protected final String CMP_TITLE = "title";
    protected final String CMP_ID = "id";
    protected final String CMP_REDIRECT = "redirect";
}
