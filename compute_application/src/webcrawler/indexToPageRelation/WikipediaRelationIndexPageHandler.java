package webcrawler.indexToPageRelation;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import webcrawler.WikipediaHandler;

import commons.pagewordrelation.IndexToPage;
import commons.pagewordrelation.PageToIndex;

public class WikipediaRelationIndexPageHandler extends WikipediaHandler{

    private boolean bText = false, bTitle = false, redirect = false;
    private Integer id = 0;
    private String title, link = LINK;
    private StringBuffer sb;
    public IndexToPage indexToPage;
    public PageToIndex pageToIndex;
    
    public WikipediaRelationIndexPageHandler(IndexToPage indexToPage, PageToIndex pageToIndex) {
    	this.pageToIndex = pageToIndex;
    	this.indexToPage = indexToPage;
    }
    
    /**
     * 
     */
    @Override
    public void startElement(String uri, String localName, String qName,
    		Attributes attributes) throws SAXException {
    	if(CMP_TEXT.equals(qName)) {
    		bText = true;
    		sb = new StringBuffer();
    	} else if(CMP_REDIRECT.equals(qName)) {
    		redirect = true;
    	} else if(CMP_TITLE.equals(qName)) {
    		bTitle = true;
    		sb = new StringBuffer();
    	}
    }
    
    @Override
    public void endElement(String uri, String localName, String qName) {
    	if(CMP_TEXT.equals(qName)) {

    		//Fill the indexToPage and pageToIndex Hashmap, and ignore page
    		//with those specific word and redirection page, and generate an 
    		//id for each page
    		if(!title.contains("Catégorie:") && !title.contains("Modèle:")
    				&& !title.contains("Portail:") && !title.contains("Fichier:")
    				&& !title.contains("Projet:") && !title.contains("Module:")
    				&& !title.contains("Sujet:") && !title.contains("Wikipédia:")
    				&& !title.contains("MediaWiki:") && !title.contains("Référence:")
    				 && !redirect) {
    			id++;
    			link += title.replaceAll(" ", "_");
    			indexToPage.set(id, link);
    			pageToIndex.set(link, id);
    		}

    		link = LINK;
    		bText = false;
    	} else if(CMP_TITLE.equals(qName)) {
    		title = sb.toString();
    		bTitle = false;
    		redirect = false;
    	}
    }
    
    /**
     * 
     */
    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
    	if(bText) {
    		sb.append(new String(ch, start, length));
      	} else if(bTitle) {
    		sb.append(new String(ch, start, length));
    	}
    }
}