package webcrawler.indexToPageRelation;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import commons.pagewordrelation.IndexToPage;
import commons.pagewordrelation.PageToIndex;
import commons.pagewordrelation.SimpleIndexToPage;
import commons.pagewordrelation.SimplePageToIndex;

public class MainCreateRelationIndexPage {
	public static void main(String[] args) {		
		if (args.length != 1) {
			System.err.println("waiting xml filepath to parse");
			return;
		}
		System.out.println("Start parsing");
		long start = System.nanoTime();
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		PageToIndex pageToIndex = new SimplePageToIndex();
		IndexToPage indexToPage = new SimpleIndexToPage();

		try {
			InputStream inputXML = new FileInputStream(args[0]);
			SAXParser parser = factory.newSAXParser();
			WikipediaRelationIndexPageHandler handler = 
					new WikipediaRelationIndexPageHandler(indexToPage, pageToIndex);

			parser.parse(inputXML, handler);
			handler.indexToPage.write("indexToPage.ser");
			handler.pageToIndex.write("pageToIndex.ser");
			
		} catch (Throwable e) {
			e.printStackTrace();
		}

		long end = System.nanoTime();
		System.out.println("total : " + ((end - start) / 1000000) + "ms");
		System.out.println("End parsing");
	}
}