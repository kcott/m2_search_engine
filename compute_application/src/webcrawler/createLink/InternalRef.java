package webcrawler.createLink;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import commons.math.matrix.CLI;
import commons.pagewordrelation.PageToIndex;

public class InternalRef implements Link {
	private final String LINK = "https://fr.wikipedia.org/wiki/";
	private String link = "https://fr.wikipedia.org/wiki/";
    private ArrayList<String> internalLink = null;
    
    private final String patternInternalRef = "\\{\\{(.*?)\\}\\}";
    private final String patternSeparatorRef = "\\|";
    private final String patternDelimitorRef = "\\{\\{|\\}\\}";
    
    private Pattern internalRefPattern = Pattern.compile(patternInternalRef);
    private Pattern separatorRef = Pattern.compile(patternSeparatorRef);
    private Pattern delimitor = Pattern.compile(patternDelimitorRef);

    private String[] ref1, ref2;
    private String title;
    private Matcher m;
    private int length;
    private Integer idLink;
    
    /**
     * Load in the CLI matrix all internal reference of a Wikipedia 
     * page which is represented by "{{ }}" in the XML.
     * 
     * @param contents contains the contents of a Wikipedia page.
     * @param id corresponds to the id of the current page.
     * @param pageToIndex corresponds to an hashmap which give you
     * the id of a link
     * @param cli corresponds to the cli matrix in which we will
     * load our data
     */
     public void computeLink(String contents, Integer id, 
    		PageToIndex pageToIndex, CLI cli) {
    	m = internalRefPattern.matcher(contents);

    	this.internalLink = new ArrayList<String>();

		while(m.find()) {
			title = m.group();

			//Find the internal link by removing the "{{}}" and splitting the word by
			//"|" and "#" and excluding link with particular word.
			if(title.contains("{{Article détaillé") || title.contains("{{voir")) {
				ref1 = delimitor.split(title);
				ref2 = separatorRef.split(ref1[1]);

				for(int i = 1; i < ref2.length; i++) {
					link += ref2[i].replace(" ", "_");

					if(!internalLink.contains(link) && !ref2[1].contains("contenu=")
							&& !ref2[1].contains("{{!}}") && !ref2[1].contains("amorce=")
							&& !ref2[1].contains(":Catégorie:") && !ref2[1].contains("b:")) {
						internalLink.add(link);
					}

					link = LINK;
				}
			}
		}
		
		length = internalLink.size();

		//Load the data contained in internalLink into the CLI
		for(int i = 0; i < length; i++) {
			 if((idLink = pageToIndex.get(internalLink.get(i))) != null)
				 cli.addContent(id, idLink, (1 / length));
		}

		internalLink = null;
	}
}