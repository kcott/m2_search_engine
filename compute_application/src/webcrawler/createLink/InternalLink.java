package webcrawler.createLink;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import commons.math.matrix.CLI;
import commons.pagewordrelation.PageToIndex;

public class InternalLink implements Link {
	private final String LINK = "https://fr.wikipedia.org/wiki/";
	private String link = "https://fr.wikipedia.org/wiki/";
    private ArrayList<String> internalLink = null;
    
    private final String patternInternalLink = "\\[\\[(.*?)\\]\\]";
    private final String patternSeparatorLink = "\\||#";
    private final String patternDelimitorLink = "\\[\\[|\\]\\]";
    
    private Pattern internalLinkPattern = Pattern.compile(patternInternalLink);
    private Pattern separatorLink = Pattern.compile(patternSeparatorLink);
    private Pattern delimitor = Pattern.compile(patternDelimitorLink);
	
    private Matcher m;
    private String title;
    private String[] link1, link2;
    private int length;
    private Integer idLink;
    
    /**
     * Load in the CLI matrix all internal link of a Wikipedia 
     * page which is represented by "[[ ]]" in the XML.
     * 
     * @param contents contains the contents of a Wikipedia page.
     * @param id corresponds to the id of the current page.
     * @param pageToIndex corresponds to an hashmap which give you
     * the id of a link
     * @param cli corresponds to the cli matrix in which we will
     * load our data
     */
    public void computeLink(String contents, Integer id, 
    		PageToIndex pageToIndex, CLI cli) {
    	m = internalLinkPattern.matcher(contents);

    	this.internalLink = new ArrayList<String>();
    	
		while(m.find()) {
			title = m.group();
			
			//Find the internal link by removing the "[[]]" and splitting the word by
			//"|" and "#" and excluding link with particular word.
			if(!title.equals("[[]]") && !title.equals("[[|]]") && !title.equals("[[#]]")
					&& !title.equals("[[#|]]") && !title.contains("Fichier:") 
					&& !title.contains("Catégorie:") && !title.contains("Utilisateur:") 
					&& !title.contains("Wikipédia:") && !title.contains("Image:") 
					&& !title.contains("Discussion utilisateur:")) {
				link1 = delimitor.split(title);
				link2 = separatorLink.split(link1[1]);
				System.out.println(title + " > " + link1[1] + " > ");
				System.out.println(link2[0] + "=======================================");
				link += link2[0].replace(" ", "_");
			
				if(!internalLink.contains(link)) {
					internalLink.add(link);
				}
				
				link = LINK;
			}
		}

		length = internalLink.size();
		
		//Load the data contained in internalLink into the CLI
		for(int i = 0; i < length; i++) {
		    if((idLink = pageToIndex.get(internalLink.get(i))) != null)
				cli.addContent(id, idLink, (1 / length));
		}
		
		internalLink = null;
	
    }
}