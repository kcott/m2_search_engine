package webcrawler.createLink;

import commons.math.matrix.CLI;
import commons.pagewordrelation.PageToIndex;

public interface Link {
	public void computeLink(String contents, Integer id, 
			PageToIndex pageToIndex, CLI cli);
}
