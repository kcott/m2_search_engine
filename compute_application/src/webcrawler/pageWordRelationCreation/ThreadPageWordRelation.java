package webcrawler.pageWordRelationCreation;

import webcrawler.Parser;

import commons.pagewordrelation.PageWordsRelation;

public class ThreadPageWordRelation extends Thread {
	private Parser parser;
	private PageWordsRelation pageWordsRelation;
	private Integer id;
	private String contents, fileIn;
	
	public ThreadPageWordRelation(Parser parser, PageWordsRelation pageWordsRelation,
			Integer id, String contents, String fileIn) {
		this.parser = parser;
		this.pageWordsRelation = pageWordsRelation;
		this.id = id;
		this.contents = contents;
		this.fileIn = fileIn;
		this.start();
	}
	
	@Override
	public void run() {
	//	parser.createWordPageRelation(fileIn, contents, id, pageWordsRelation);
	}
}
