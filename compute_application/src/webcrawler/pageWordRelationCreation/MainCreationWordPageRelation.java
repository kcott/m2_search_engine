package webcrawler.pageWordRelationCreation;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import webcrawler.dictionary.Dictionary;

import commons.pagewordrelation.PageToIndex;
import commons.pagewordrelation.SimplePageToIndex;

public class MainCreationWordPageRelation {
	public static void main(String[] args) {		
		if (args.length != 2) {
			System.err.println("waiting xml filepath to parse");
			return;
		}

		System.out.println("Start parsing");
		long start = System.nanoTime();

		//PageWordsRelation pageWordsRelation = new ArrayListPageWordsRelation();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		
		try {
			InputStream inputXML = new FileInputStream(args[0]);
			String fileIn = args[1];
			SAXParser parser = factory.newSAXParser();
			PageToIndex pageToIndex = new SimplePageToIndex();
			Dictionary dictionary = new Dictionary(fileIn);		
				
			pageToIndex.read("pageToIndex.ser");
			
/*			WikipediaPageWordRelationHandler handler = 
					new WikipediaPageWordRelationHandler(pageWordsRelation, 
							pageToIndex, fileIn);
*/
			WikipediaPageWordRelationHandler handler = 
					new WikipediaPageWordRelationHandler(dictionary, 
							pageToIndex, fileIn);

			parser.parse(inputXML, handler);
			dictionary.write("pageWordRelation.ser");
			//pageWordsRelation.write("pageWordRelation.ser");
		} catch (Throwable e) {
			e.printStackTrace();
		}

		long end = System.nanoTime();
		System.out.println("total : " + ((end - start) / 1000000) + "ms");
		System.out.println("End parsing");
	}
}