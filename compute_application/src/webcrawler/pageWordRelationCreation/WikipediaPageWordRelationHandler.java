package webcrawler.pageWordRelationCreation;

import java.text.Normalizer;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import webcrawler.Parser;
import webcrawler.WikipediaHandler;
import webcrawler.createLink.InternalLink;
import webcrawler.dictionary.Dictionary;

import commons.pagewordrelation.PageToIndex;

public class WikipediaPageWordRelationHandler extends WikipediaHandler {

    private boolean bText = false, bTitle = false;
    private String contenu, title, link = LINK, fileIn;
    private StringBuffer sb;
    private Parser parser;
    private Integer id;
//    private ThreadPageWordRelation thread;
    
    private final String REPLACE_INPUT = "\n";
    private final String REPLACE_OUTPUT = "";
    private final String NORMALIZE_INPUT = "\\p{InCombiningDiacriticalMarks}+";
    private final String NORMALIZE_OUTPUT = "";
    
//    PageWordsRelation pageWordsRelation = null;
    Dictionary dictionary = null;
    PageToIndex pageToIndex = null;
    
    /*public WikipediaPageWordRelationHandler(PageWordsRelation pageWordsRelation, 
    		PageToIndex pageToIndex, String fileIn) {
    	this.fileIn = fileIn;
    	this.pageWordsRelation = pageWordsRelation;
    	this.pageToIndex = pageToIndex;
    }*/
    
    public WikipediaPageWordRelationHandler(Dictionary dictionary, 
    		PageToIndex pageToIndex, String fileIn) {
    	this.fileIn = fileIn;
    	this.dictionary = dictionary;
    	this.pageToIndex = pageToIndex;
    }
    
    /**
     * 
     */
    @Override
    public void startElement(String uri, String localName, String qName,
    		Attributes attributes) throws SAXException {
    	if(CMP_TEXT.equals(qName)) {
    		bText = true;
    		sb = new StringBuffer();
    	} else if(CMP_TITLE.equals(qName)) {
    		bTitle = true;
    		sb = new StringBuffer();
    	}
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
    	if(CMP_TEXT.equals(qName)) {
    		link += title.replaceAll(" ","_");
    		
    		//remove special characters from the content of the string
    		//and \n and then convert it to lower case
    		contenu = null;
    		contenu = sb.toString();
    		contenu = contenu.replaceAll(REPLACE_INPUT, REPLACE_OUTPUT);
    		contenu = Normalizer.normalize(contenu, Normalizer.Form.NFD)
					.replaceAll(NORMALIZE_INPUT, NORMALIZE_OUTPUT);
    		contenu = contenu.toLowerCase();

    		//Get the id of the current page
    		id = pageToIndex.get(link);

    		if(id != null) {
    			//Create a new parser, and create and fill the word-page relation
    			parser = new Parser(new InternalLink());
    			//parser.createWordPageRelation(fileIn, contenu, id, pageWordsRelation);
    			parser.createWordPageRelation(fileIn, contenu, id, dictionary);
    			
    			/*try {
    				thread = new ThreadPageWordRelation(parser, pageWordsRelation, id, contenu, fileIn);
    				thread.join();
    			} catch (InterruptedException e) {
    				e.printStackTrace();
    			}*/
    		}
    		
    		link = LINK;
    		bText = false;
    	} else if(CMP_TITLE.equals(qName)) {
    		title = sb.toString();
    		bTitle = false;
    	} else if(CMP_PAGE.equals(qName)) {
    	}
    }
    
    /**		
     * 
     */
    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
    	if(bText) {
    		sb.append(new String(ch, start, length));
      	} else if(bTitle) {
    		sb.append(new String(ch, start, length));
    	} 
    }
}