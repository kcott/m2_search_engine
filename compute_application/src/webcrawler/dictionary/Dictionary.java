package webcrawler.dictionary;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class Dictionary implements Serializable {
	private static final long serialVersionUID = 1L;
	private Map<String, List<Integer>> dictionary = new HashMap<String, List<Integer>>();
	private List<Integer> ids = new ArrayList<Integer>();
	
	public Dictionary(String filename) {
		fill(filename);
	}
	
	public void put(String word, Integer id) {
		if (!dictionary.containsKey(word))
			dictionary.put(word, new ArrayList<Integer>());
		dictionary.get(word).add(id);
	}
	
	public boolean contains(String word) {
		return dictionary.containsKey(word);
	}
	
	public void fill(String filename) {
		try {
			String line;
			InputStream is = new FileInputStream(filename);
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);

			while ((line = br.readLine()) != null) {
				dictionary.put(line, ids);
			}

			br.close();
			isr.close();
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void write(String filename) {
		OutputStreamWriter osw = null;
		
		try {
			final FileOutputStream file = new FileOutputStream(filename);
			osw = new OutputStreamWriter(file, "UTF-8");
			osw.write("" + serialVersionUID + "\n");
			
			for(Entry<String, List<Integer>> entry: dictionary.entrySet()) {
				osw.write(entry.getKey() + " ");
				for(Integer id: entry.getValue()) {
					osw.write(id + " ");
				}
				osw.write("\n");
				osw.flush();
			}		
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(osw != null) {
					osw.flush();
					osw.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}		
		}
	}
	
	public void read(String filename) {
		dictionary = new HashMap<String, List<Integer>>();
		BufferedReader reader = null;

		try {
			final FileInputStream file = new FileInputStream(filename);
			reader = new BufferedReader(new InputStreamReader(file, "UTF-8"));
			long version = Long.parseLong(reader.readLine());
			String line;
			int length;
			ArrayList<Integer> value = null;
			
			while((line = reader.readLine()) != null){
				length = line.split(" ").length;
				value = new ArrayList<Integer>(length);
				
				for(int i = 1; i < length; i++) {
					value.add(Integer.parseInt(line.split(" ")[i]));
				}
				
				dictionary.put(line.split(" ")[0], value);
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {}
		}
	}
}
