package webcrawler.dictionary;

public interface CleanerRawDatas {
	public void clean(String inputFile, String outputFile, String emptywordFile);
}
