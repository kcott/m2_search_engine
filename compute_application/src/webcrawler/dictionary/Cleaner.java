package webcrawler.dictionary;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.Normalizer;

public class Cleaner implements CleanerRawDatas {
	/**
	 * This method will take a dictionary and remove all empty word, then
	 * remove special characters, and finally converts all word to a lower
	 * case
	 * 
	 * @param inputFile corresponds to the dictionary that we will work with.
	 * @param outputFile corresponds to the dictionary that we obtain after
	 * normalizing it
	 * @param emptywordFile corresponds to the file containing the list of
	 * all the empty words
	 */
	@Override
	public void clean(String inputFile, String outputFile, String emptywordFile) {
		String line, pattern = "";

		try {
			InputStream is = new FileInputStream(inputFile);
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			InputStream is2 = new FileInputStream(emptywordFile);
			InputStreamReader isr2 = new InputStreamReader(is2);
			BufferedReader br2 = new BufferedReader(isr2);
			OutputStream os = new FileOutputStream(outputFile);
			OutputStreamWriter osw = new OutputStreamWriter(os);
			BufferedWriter bw = new BufferedWriter(osw);

			//We will read emptywordFile and concatenate
			//all its elements separated by a |
			while((line = br2.readLine()) != null) {
				pattern += line + "|";
			}
			
			//We will read the dictionary and see if the current word is an empty 
			//word. If not we will remove the accent and converts it to lower case
			//before writing it on the outputFile
			while((line = br.readLine()) != null) {			
				if(!line.matches(pattern) && line.length() > 2) {
					line = Normalizer.normalize(line, Normalizer.Form.NFD)
							.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
					line = line.toLowerCase();
					bw.write(line + "\n");
					bw.flush();
				}
			}

			is.close();
			isr.close();
			br.close();
			is2.close();
			isr2.close();
			br2.close();
			os.close();
			osw.close();
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}