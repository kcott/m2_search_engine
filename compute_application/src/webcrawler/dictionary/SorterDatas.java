package webcrawler.dictionary;

import java.io.File;
import java.io.IOException;

import commons.SystemCall;

public class SorterDatas {
	/**
	 * This method take a dictionary that have been cleaned and will
	 * remove all redundant word and then sort the dictionary
	 * 
	 * @param outputFilepath corresponds to the dictionary that
	 * we are going to work with
	 */
	public void sorted(String outputFilepath) throws IOException {
		String tmpOutput = outputFilepath + "_tmp";
		File tmpFile = new File(tmpOutput);
		tmpFile.createNewFile();

		String command1 = "sort -ibu " + outputFilepath + " -o " + tmpOutput;
		SystemCall.getInstance().executeAndWait(command1);
		
		String command2 = "mv " + tmpOutput + " " + outputFilepath;
		SystemCall.getInstance().executeAndWait(command2);
	}
}