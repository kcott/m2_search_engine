package webcrawler.dictionary;

import java.io.File;
import java.io.IOException;

public class Main {
	public static void main(String[] argv) {
		// Test arguments 
		if(argv.length != 3) {
			System.err.println("Usage error:");
			System.err.println("\tString rawFile:file with raw datas");
			System.err.println("\tString outputFile");
			
			return;
		}
		
		// Define the input and the output
		String inputFilepath = argv[0];
		String outputFilepath = argv[1];
		String emptywordFilepath = argv[2];
		
		// Create the output if not exists
		try {
			File f = new File(outputFilepath);
			f.createNewFile();
		} catch(Exception e) {
			e.printStackTrace();

			return;
		}

		System.out.println("Start cleaning dictionary");
		
		// Cleans raw datas
		CleanerRawDatas cleaner = new Cleaner();
		
		cleaner.clean(inputFilepath, outputFilepath, emptywordFilepath);
		
		System.out.println("End cleaning dictionary");
		System.out.println("Start sorting dictionary");

		// Sorts datas
		SorterDatas sortedDatas = new SorterDatas();
		
		try {
			sortedDatas.sorted(outputFilepath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("End sorting dictionary");
	}
}