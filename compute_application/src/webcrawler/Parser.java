																																																																																																																																																																																																																																						package webcrawler;

import webcrawler.createLink.Link;
import webcrawler.dictionary.Dictionary;

import commons.math.matrix.CLI;
import commons.pagewordrelation.PageToIndex;

public class Parser {
	private Link link;
	
	public Parser(Link link) {
		this.link = link;
	}

    /**
     * Load in the CLI matrix all link of a Wikipedia 
     * page which is represented by "[[ ]]" or "{{ }}" in the XML.
 
     * 
     * @param contents contains the contents of a Wikipedia page.
     * @param id corresponds to the id of the current page.
     * @param pageToIndex corresponds to an hashmap which give you
     * the id of a link
     * @param cli corresponds to the cli matrix in which we will
     * load our data
     */
	public void computeLink(String contents, Integer id, 
			PageToIndex pageToIndex, CLI cli) {
		link.computeLink(contents, id, pageToIndex, cli);
	}

	/**
	 * Create the word-page relation by searching in the contents of a 
	 * page the word from the dictionary 
	 * 
	 * @param fileIn is the link of dictionary file
	 * @param pageWordsRelation is the hashmap which will have for key
	 * a word and for value an ArrayList of id 
	 * @param contents contains the contents of the Wikipedia page
	 * @param id is the id of the wikipedia page
	 */
	public void createWordPageRelation(String fileIn, String contents, 
			Integer id, /*PageWordsRelation pageWordsRelation*/Dictionary dictionary) {
/*		try {
			String line;
			InputStream is = new FileInputStream(fileIn);
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);

			while ((line = br.readLine()) != null) {
				if(contents.contains(line)) {
					pageWordsRelation.add(line, id);
				}
			}

			br.close();
			isr.close();
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		String[] tmp = contents.split("\\W");
		
		for(int i = 0; i < tmp.length; i++) {
			System.out.println(tmp[i]);
			if(dictionary.contains(tmp[i])) {
				dictionary.put(tmp[i], id);
			}
		}
	}
	
	public void setLinkStrategy(Link link) {
		this.link = link;
	}
}