package system;

public class SystemConfiguration {
	private final String FILE = "system.conf"; 
	/*
	 * Singleton
	 */
	private static SystemConfiguration instance;
	public static SystemConfiguration getInstance(){
		if(instance == null)
			instance = new SystemConfiguration();
		return instance;
	}
	/*
	 * Real class
	 */
	ReaderConfiguration configuration;
	private SystemConfiguration(){
		configuration = new ReaderConfiguration(FILE);
	}

	public String getLang() {
		return configuration.getConf("lang");
	}
	public String getConfExtentions() {
		return configuration.getConf("confsExtention");
	}
	
	public String getConfsDirectory() {
		return configuration.getConf("confsDirectory");
	}
	public String getDatasDirectory() {
		return configuration.getConf("datasDirectory");
	}
	public String getDictsDirectory() {
		return configuration.getConf("dictsDirectory");
	}
	public String getWordsDirectory() {
		return configuration.getConf("wordsDirectory");
	}
	public String getEmptyWordsDirectory() {
		return configuration.getConf("emptyWordsDirectory");
	}
	public String getOccurencesDirectory() {
		return configuration.getConf("occurencesDirectory");
	}
}
