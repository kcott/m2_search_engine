/**
 * TP n:6
 * 
 * Titre du TP:Jointure en m�moire externe
 * 
 * Date:2015-11-19
 * 
 * Nom:Cottin
 * Prenom:Kevin
 * 
 * email:cottin@informatique.univ-paris-diderot.fr
 * 
 * Remarques:
 */
package system;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Read a file configuration<br/>
 * A line beginning with "#" is a comment<br/>
 * A line well-formed is under form "key = value"
 */
public class ReaderConfiguration {
	private Map<String,String> confs;
	/**
	 * Create the reader to read a configuration
	 */
	public ReaderConfiguration(String confFile) {
		confs = new HashMap<String,String>();
		FileInputStream fis;
		try {
			fis = new FileInputStream(confFile);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line;
			while((line = br.readLine()) != null) {
				if(line.startsWith("#") || line.trim().equals(""))
					continue;
				String params[] = line.split("=");
				confs.put(params[0].trim(), params[1].trim());
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	/**
	 * Get the configuration of the key
	 * @param key
	 * @return
	 */
	public String getConf(String key) {
		return confs.get(key);
	}
	/**
	 * Get all configuration keys
	 * @return String[]
	 */
	public String[] getKeys() {
		String[] keys = new String[confs.size()];
		int n = 0;
		for (String key:confs.keySet()) {
			keys[n] = key;
			n++;
		}
		return keys;
	}
}
